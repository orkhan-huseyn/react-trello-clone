import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import AddTodoModal from './components/AddTodoModal';
import TodoColumn from './components/TodoColumn';
import TodoItem from './components/TodoItem';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      backlog: [],
      inProgress: [],
      inReview: [],
      completed: [],
    };

    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleAddTodo = this.handleAddTodo.bind(this);
    this.handleDeleteTodo = this.handleDeleteTodo.bind(this);
    this.handleMoveToInProgress = this.handleMoveToInProgress.bind(this);
  }

  handleOpenModal() {
    this.setState({
      isModalOpen: true,
    });
  }

  handleCloseModal() {
    this.setState({
      isModalOpen: false,
    });
  }

  handleAddTodo(value) {
    const backlog = this.state.backlog.slice();
    backlog.push({
      id: uuidv4(),
      title: value,
    });
    this.setState({
      backlog,
      isModalOpen: false,
    });
  }

  handleDeleteTodo(idToDelete) {
    const backlog = this.state.backlog.filter((item) => item.id !== idToDelete);
    this.setState({
      backlog,
    });
  }

  handleMoveToInProgress(itemId) {
    const item = this.state.backlog.find((item) => item.id === itemId);
    const backlog = this.state.backlog.filter((item) => item.id !== itemId);
    const inProgress = this.state.inProgress.slice();
    inProgress.push(item);

    this.setState({
      backlog,
      inProgress,
    });
  }

  render() {
    const { isModalOpen, backlog, inProgress } = this.state;

    return (
      <>
        <AddTodoModal
          isModalOpen={isModalOpen}
          onClose={this.handleCloseModal}
          onSubmit={this.handleAddTodo}
        />
        <div className="todo-container">
          <TodoColumn title="Backlog">
            <button onClick={this.handleOpenModal} id="addBtn">
              + Add Todo
            </button>
            {backlog.map((item) => (
              <TodoItem
                id={item.id}
                key={item.id}
                title={item.title}
                onDelete={this.handleDeleteTodo}
              />
            ))}
          </TodoColumn>
          <TodoColumn onDrop={this.handleMoveToInProgress} title="In Progress">
            {inProgress.map((item) => (
              <TodoItem
                id={item.id}
                key={item.id}
                title={item.title}
                onDelete={this.handleDeleteTodo}
              />
            ))}
          </TodoColumn>
          <TodoColumn title="In Review"></TodoColumn>
          <TodoColumn title="Completed"></TodoColumn>
        </div>
      </>
    );
  }
}

export default App;
