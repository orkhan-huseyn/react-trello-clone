import React from 'react';

function AddTodoModal({ isModalOpen, onClose, onSubmit }) {
  function handleSubmit(event) {
    event.preventDefault();
    const { todoInput } = event.target.elements;
    onSubmit(todoInput.value);
    todoInput.value = '';
  }

  return (
    <>
      <div className={isModalOpen ? 'modal active' : 'modal'} id="todoForm">
        <div className="header">
          <div className="title">Add Todo</div>
          <button onClick={onClose} className="btn close-modal">
            &times;
          </button>
        </div>
        <form
          onSubmit={handleSubmit}
          autoComplete="off"
          id="addTodoForm"
          className="body"
        >
          <input type="text" id="todoInput" aria-label="Add todo item..." />
          <button id="todoSubmit">Add Todo</button>
        </form>
      </div>
      <div id="overlay" className={isModalOpen ? 'active' : ''}></div>
    </>
  );
}

export default AddTodoModal;
