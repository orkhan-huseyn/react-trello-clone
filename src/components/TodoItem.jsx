import React from 'react';

function TodoItem({ id, title, onDelete }) {
  function handleDrag(event) {
    event.dataTransfer.setData('text/plain', id);
  }

  return (
    <div className="todo" onDragStart={handleDrag} draggable>
      {title}
      <button onClick={() => onDelete(id)} className="close">
        &times;
      </button>
    </div>
  );
}

export default TodoItem;
