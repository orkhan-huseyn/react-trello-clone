import React from 'react';

function TodoColumn({ onDrop, title, children }) {
  function handleDragOver(event) {
    event.preventDefault();
  }

  function handleDrop(event) {
    const itemId = event.dataTransfer.getData('text/plain');
    onDrop(itemId);
  }

  return (
    <div onDragOver={handleDragOver} onDrop={handleDrop} className="status">
      <h1>{title}</h1>
      {children}
    </div>
  );
}

export default TodoColumn;
